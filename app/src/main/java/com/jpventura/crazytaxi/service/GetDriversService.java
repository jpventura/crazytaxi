package com.jpventura.crazytaxi.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;

import com.jpventura.crazytaxi.backend.Backend;
import com.jpventura.crazytaxi.backend.Driver;
import com.jpventura.crazytaxi.backend.IBackend;
import com.jpventura.crazytaxi.content.driver.DriverContentValues;

public class GetDriversService extends IntentService {
    private static final String ACTION_SYNC = "com.jpventura.crazytaxi.service.action.SYNC";

    public static final String EXTRA_SW = "com.jpventura.crazytaxi.service.extra.SOUTHWEST";
    public static final String EXTRA_NE = "com.jpventura.crazytaxi.service.extra.NORTHEAST";

    public static synchronized void execute(Context context, Bundle bundle) {
        Intent intent = new Intent(context, GetDriversService.class);
        intent.setAction(ACTION_SYNC);
        intent.putExtra("location", bundle);
        context.startService(intent);
    }

    public static synchronized void execute(Context context, String southwest, String northeast) {


        Intent intent = new Intent(context, GetDriversService.class);
        intent.setAction(ACTION_SYNC);
        intent.putExtra(EXTRA_SW, southwest);
        intent.putExtra(EXTRA_NE, northeast);
        context.startService(intent);
    }

    public GetDriversService() {
        super("GetDriversService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle bundle = intent.getBundleExtra("location");

        if (null == bundle) {
            String southwest = intent.getStringExtra(EXTRA_SW);
            String northeast = intent.getStringExtra(EXTRA_NE);
            handleGetDriversList(southwest, northeast);
        } else {
            Bundle southwest = intent.getBundleExtra("location").getBundle("southwest");
            Bundle northeast = intent.getBundleExtra("location").getBundle("northeast");
            handleGetDriversList(southwest.getString("latitude") + "," + southwest.getString("longitude"),
                    northeast.getString("latitude") + "," + northeast.getString("longitude"));
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleGetDriversList(String southwest, String northeast) {
        IBackend backend = Backend.getInstance();
        for (Driver driver : backend.getDrivers(southwest, northeast)) {
            DriverContentValues values = new DriverContentValues().putValues(driver);
            values.insert(this);
        }
    }
}
