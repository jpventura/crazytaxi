/*
 * Copyright 2015 Joao Paulo Fernandes Ventura.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jpventura.crazytaxi.backend;

import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.http.Query;

public class Backend implements IBackend {
    private static IBackend sBackend = null;
    private static final Object lock = new Object();

    private static final String REGEX_LAT_LNG = "[-+]?[0-9]*\\.?[0-9]+,[-+]?[0-9]*\\.?[0-9]+";
    private static final Pattern sPattern = Pattern.compile(REGEX_LAT_LNG);

    private IBackend mBackend;

    public static IBackend getInstance() {
        if (null == sBackend) {
            synchronized (lock) {
                sBackend = new Backend();
            }
        }

        return sBackend;
    }

    @Override
    public List<Driver> getDrivers(@Query("sw") String southwest,
                                   @Query("ne") String northeast) {

        if ((null == southwest) || (null == northeast)) {
            return null;
        }

        if (!isCoordinateLegal(southwest) || !isCoordinateLegal(northeast)) {
            return null;
        }

        return mBackend.getDrivers(southwest, northeast);

    }

    @Override
    public void getDriversInBackground(@Query("sw") String southwest,
                                       @Query("ne") String northeast,
                                       GetDriversCallback callback) {
        mBackend.getDriversInBackground(southwest, northeast, callback);
    }

    private boolean isCoordinateLegal(String coordinate) {
        return sPattern.matcher(coordinate).matches();
    }

    private Backend() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(IBackend.REST_ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        mBackend = restAdapter.create(IBackend.class);
    }
}
