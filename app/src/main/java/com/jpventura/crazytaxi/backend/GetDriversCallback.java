package com.jpventura.crazytaxi.backend;

import java.util.List;

import retrofit.Callback;
import retrofit.client.Response;

public interface GetDriversCallback extends Callback<List<Driver>> {
    @Override
    void success(List<Driver> drivers, Response response);
}
