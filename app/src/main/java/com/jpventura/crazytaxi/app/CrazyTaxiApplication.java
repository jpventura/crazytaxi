package com.jpventura.crazytaxi.app;

import android.app.Application;

import com.jpventura.crazytaxi.content.TaxiSyncAdapter;
import com.jpventura.crazytaxi.controller.LocationController;

public class CrazyTaxiApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        TaxiSyncAdapter.initializeSyncAdapter(this);
    }
}
