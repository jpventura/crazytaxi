package com.jpventura.crazytaxi.app;

import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jpventura.crazytaxi.R;
import com.jpventura.crazytaxi.content.driver.DriverColumns;
import com.jpventura.crazytaxi.content.driver.DriverCursor;
import com.jpventura.crazytaxi.content.driver.DriverSelection;
import com.jpventura.crazytaxi.controller.LatLngBoundsWrapper;
import com.jpventura.crazytaxi.controller.LocationController;
import com.jpventura.crazytaxi.controller.RefreshMapController;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity
        implements LoaderCallbacks<Cursor>, OnCameraChangeListener, OnMapReadyCallback, Observer {
    private static final double ONE_KM = 0.000009013660470958916;
    private static final double ONE_HUNDRED_METERS = ONE_KM/10;
    private static final int REQUEST_LOCATION = 42;

    private GoogleMap mMap;

    private LatLng mLatLng;

    private LocationController mLocationController;

    private RefreshMapController mRefreshMapController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        LocationController.create(this);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapFragment.setRetainInstance(true);

        mLocationController = LocationController.getInstance();
        mLocationController.onCreate(null);

        mRefreshMapController = RefreshMapController.getInstance(this);
        LatLngBoundsWrapper wrapper =
                LatLngBoundsWrapper.create(mLocationController.getLastLocation());
        mRefreshMapController.onCreate(wrapper.getBundle());

        getLoaderManager().initLoader(0, wrapper.getBundle(), this);
    }

    public void onCameraChange(CameraPosition var1) {
        if ((null == mMap) || (null == mRefreshMapController)) return;

        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        mRefreshMapController.onResume(getVisibleRegion(bounds));

        Bundle bundle = LatLngBoundsWrapper.create(bounds).getBundle();
        getLoaderManager().restartLoader(0, bundle, this);

        mLatLng = var1.target;
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args) {
        DriverSelection selection = new DriverSelection()
                .latitudeGtEq(args.getDouble("south")).and().latitudeLtEq(args.getDouble("north"));

        return new CursorLoader(
                this,
                selection.uri(),
                DriverColumns.ALL_COLUMNS,
                selection.sel(),
                selection.args(),
                null
        );
    }

    private Map<Long, Marker> mMarkerMap = new HashMap();



    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if ((null == mMarkerMap) || (null == data) || (data.getCount() == 0)) return;

        onUpdateMapMarkers(new DriverCursor(data));
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        mMap.clear();
        mMap.setOnCameraChangeListener(this);
        mMap.setMyLocationEnabled(true);

        if (mLatLng == null) return;

        CameraPosition target = CameraPosition.builder().target(mLatLng).zoom(17).build();
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(target));

        LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        LatLngBoundsWrapper wrapper = LatLngBoundsWrapper.create(bounds);
        mRefreshMapController.onResume(wrapper.getBundle());

        getLoaderManager().restartLoader(0, wrapper.getBundle(), this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access location data.", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mLocationController.addObserver(this);
        mLocationController.onResume(null);

        if (null != mMap) {
            LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
            LatLngBoundsWrapper wrapper = LatLngBoundsWrapper.create(bounds);
            mRefreshMapController.onResume(wrapper.getBundle());
        }

    }

    @Override
    protected void onStop() {
        mLocationController.deleteObserver(this);
        mLocationController.onStop();
        mRefreshMapController.onStop();
        super.onStop();
    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable == mLocationController) {
            LatLng location = (LatLng) data;
            mLatLng = new LatLng(location.latitude, location.longitude);
        }
    }

    private Bundle getVisibleRegion(LatLngBounds bounds) {
        return LatLngBoundsWrapper.create(bounds).getBundle();
    }

    /**
     * Detect if the user has moved significantly (i.e. at least 20 meters). The taxi coordinates
     * seem to be randomly generated, so the free are flickering on the screen when moved.
     *
     * @param before Previous user location.
     * @param after Current user location.
     * @return true if move was significantly, false otherwise.
     */
    private boolean hasMovedSignificantly(LatLng before, LatLng after) {
        if (Math.abs(before.latitude - after.latitude) >= ONE_HUNDRED_METERS) {
            return true;
        }

        if (Math.abs(before.longitude - after.longitude) >= ONE_HUNDRED_METERS) {
            return true;
        }

        return false;
    }

    private void onUpdateMapMarkers(DriverCursor cursor) {
        if ((null == cursor) || (cursor.getCount() == 0)) return;

        if (null == mMarkerMap) return;

        cursor.moveToFirst();
        do {
            LatLng position = new LatLng(cursor.getLatitude(), cursor.getLongitude());

            if (mMarkerMap.containsKey(cursor.getDriverId())) {
                boolean isInfoWindowShown = mMarkerMap
                        .get(cursor.getDriverId())
                        .isInfoWindowShown();

                Marker marker = mMarkerMap.get(cursor.getDriverId());
                if (hasMovedSignificantly(marker.getPosition(), position)) {
                    updateMarkerPosition(mMarkerMap.get(cursor.getDriverId()), position);
                }

                // Keep details visible if they were shown
                if (isInfoWindowShown) {
                    mMarkerMap.get(cursor.getDriverId()).showInfoWindow();
                }
            } else if (null != mMap) {
                // Add a brand new marker to the map
                BitmapDescriptor icon = BitmapDescriptorFactory
                        .fromResource(cursor.getAvailable() ? R.drawable.free : R.drawable.busy);
                MarkerOptions options = new MarkerOptions()
                        .title("ID " + Long.toString(cursor.getDriverId()))
                        .position(position)
                        .icon(icon);
                mMarkerMap.put(cursor.getDriverId(), mMap.addMarker(options));
            }

        } while (cursor.moveToNext());
    }

    /**
     * Move marker smoothly from original to new position.
     *
     * @param marker Marker on map visible region
     * @para position new marker position
     *
     * @see <a href="http://goo.gl/QO1FyY">Move markers in Google Map v2 Android</a>
     */
    private void updateMarkerPosition(final Marker marker, final LatLng position) {

        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection projection = mMap.getProjection();
        Point startPoint = projection.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = projection.fromScreenLocation(startPoint);
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);
                double lng = t * position.longitude + (1 - t) * startLatLng.longitude;
                double lat = t * position.latitude + (1 - t) * startLatLng.latitude;
                marker.setPosition(new LatLng(lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
                marker.setVisible(true);
            }
        });
    }
}
