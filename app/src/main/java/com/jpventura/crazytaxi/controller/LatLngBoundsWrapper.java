package com.jpventura.crazytaxi.controller;

import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.regex.Pattern;

public class LatLngBoundsWrapper {
    private static final Double TEN_KM_LAT = 0.00009044289887579477;
    private static final Double TEN_KM_LNG = 0.00008983031054338354;

    private static final String REGEX_LAT_LNG = "[-+]?[0-9]*\\.?[0-9]+,[-+]?[0-9]*\\.?[0-9]+";
    private static final Pattern sPattern = Pattern.compile(REGEX_LAT_LNG);


    public final LatLng mSouthwest;
    public final LatLng mNortheast;

    public static LatLngBoundsWrapper create(LatLngBounds bounds) {
        return new LatLngBoundsWrapper(bounds.southwest, bounds.northeast);
    }

    public static LatLngBoundsWrapper create(LatLng location) {
        LatLng sw = new LatLng(location.latitude - TEN_KM_LAT, location.longitude - TEN_KM_LNG);
        LatLng ne = new LatLng(location.latitude + TEN_KM_LAT, location.longitude + TEN_KM_LNG);
        return new LatLngBoundsWrapper(sw, ne);
    }

    public static LatLngBoundsWrapper create(LatLng southwest, LatLng northeast) {
        return new LatLngBoundsWrapper(southwest, northeast);
    }

    public static LatLngBoundsWrapper create(String southwest, String northeast) {
        if (!isCoordinateLegal(southwest)) {
            throw new IllegalArgumentException("Illegal southwest coordinate");
        }

        if (!isCoordinateLegal(northeast)) {
            throw new IllegalArgumentException("Illegal southwest coordinate");
        }

        String[] sw = southwest.split(",");
        String[] ne = northeast.split(",");

        return new LatLngBoundsWrapper(
                new LatLng(Double.valueOf(sw[0]), Double.valueOf(sw[1])),
                new LatLng(Double.valueOf(ne[0]), Double.valueOf(ne[1])));
    }

    public static String getSouthWest(LatLng southwest) {
        return Double.toString(southwest.latitude) + "," + Double.toString(southwest.longitude);
    }

    public static String getNorthEast(LatLng northeast) {
        return Double.toString(northeast.latitude) + "," + Double.toString(northeast.longitude);
    }

    public static Bundle getBundle(LatLng southwest, LatLng northeast) {
        Bundle region = new Bundle();
        region.putString("southwest", getSouthWest(southwest));
        region.putString("northeast", getNorthEast(northeast));
        region.putDouble("south", southwest.latitude);
        region.putDouble("west", southwest.longitude);
        region.putDouble("north", northeast.latitude);
        region.putDouble("east", northeast.longitude);
        return region;
    }

    public String getSouthWest() {
        return Double.toString(mSouthwest.latitude) + "," + Double.toString(mSouthwest.longitude);
    }

    public String getNorthEast() {
        return Double.toString(mNortheast.latitude) + "," + Double.toString(mNortheast.longitude);
    }

    public Bundle getBundle() {
        Bundle region = new Bundle();
        region.putString("southwest", getSouthWest());
        region.putString("northeast", getNorthEast());
        region.putDouble("south", mSouthwest.latitude);
        region.putDouble("west", mSouthwest.longitude);
        region.putDouble("north", mNortheast.latitude);
        region.putDouble("east", mNortheast.longitude);
        return region;
    }

    private static boolean isCoordinateLegal(String coordinate) {
        return sPattern.matcher(coordinate).matches();
    }

    private LatLngBoundsWrapper(LatLng southwest, LatLng northeast) {
        mSouthwest = southwest;
        mNortheast = northeast;
    }
}
