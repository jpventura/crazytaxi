package com.jpventura.crazytaxi.controller;

import android.os.Bundle;

public interface IController {
    void onCreate(Bundle args);

    void onResume(Bundle args);

    void onStop();
}
