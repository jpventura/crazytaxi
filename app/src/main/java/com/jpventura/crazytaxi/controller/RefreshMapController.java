package com.jpventura.crazytaxi.controller;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.jpventura.crazytaxi.service.GetDriversService;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

public class RefreshMapController implements IController {
    private static RefreshMapController sRefreshMapController;
    private static final Object lock = new Object();
    private static Timer sTimer;
    private static String sSouthWest;
    private static String sNorthEast;

    private WeakReference<Activity> mActivity;
    private final long DELAY = 0;
    private final long PERIOD = 5000;

    public static RefreshMapController getInstance(Activity activity) {
        if (null == sRefreshMapController) {
            synchronized (lock) {
                sRefreshMapController = new RefreshMapController(activity);
            }
        }

        return sRefreshMapController;
    }

    @Override
    public void onCreate(Bundle args) {
    }

    @Override
    public void onResume(Bundle args) {
        if (null == sTimer) {
            scheduleTimer();
        }
        setLatLngBounds(args);
    }

    @Override
    public void onStop() {
        cancelTimer();
    }

    private void setLatLngBounds(Bundle bounds) {
        sSouthWest = bounds.getString("southwest");
        sNorthEast = bounds.getString("northeast");
    }

    private RefreshMapController(Activity activity) {
        mActivity = new WeakReference<>(activity);
    }

    private void cancelTimer() {
        if (null == sTimer) return;
        sTimer.cancel();
        sTimer = null;
    }

    private void scheduleTimer() {
        sTimer = new Timer();
        sTimer.schedule(new RefreshTimerTask(mActivity.get()), DELAY, PERIOD);
    }

    private class RefreshTimerTask extends TimerTask {
        private WeakReference<Activity> mActivity;

        public RefreshTimerTask(Activity activity) {
            super();
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void run() {
            if ((null == sSouthWest) || (null == sNorthEast)) return;
            GetDriversService.execute(mActivity.get(), sSouthWest, sNorthEast);
        }
    }
}
