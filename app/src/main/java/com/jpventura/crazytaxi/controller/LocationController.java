package com.jpventura.crazytaxi.controller;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.jpventura.crazytaxi.R;

import java.lang.ref.WeakReference;
import java.util.Observable;

public class LocationController extends Observable
        implements IController, ConnectionCallbacks, OnConnectionFailedListener, LocationListener {
    public static final int REQUEST_ACCESS_FINE_LOCATION = 42;

    private static long SYNC_INTERVAL_MILLISECONDS = 5000;
    private static final String KEY_LAT = "latitude";
    private static final String KEY_LNG = "longitude";
    private static final String VALUE_LAT = "-23.601011";
    private static final String VALUE_LNG = "-46.687968";

    private static LocationController sLocationController;
    private static final Object lock = new Object();

    private WeakReference<Activity> mActivity;
    private GoogleApiClient.Builder mBuilder;
    private GoogleApiClient mClient;
    private LocationRequest mLocationRequest;
    private LatLng mLastLocation;

    public static void create(Activity activity) {
        if (null == sLocationController) {
            synchronized (lock) {
                sLocationController = new LocationController(activity);
            }
        }
    }

    public static LocationController getInstance() {
        return sLocationController;
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = new LatLng(location.getLatitude(), location.getLongitude());
        notifyObservers(mLastLocation);
    }

    @Override
    public void onCreate(Bundle args) {
        restoreLastLocation();
    }

    @Override
    public void onResume(Bundle args) {
        mClient.connect();
        notifyObservers(mLastLocation);
    }

    @Override
    public void onStop() {
        saveLastLocation();
        mClient.disconnect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (!isAccessFineLocationPermitted()) return;

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(SYNC_INTERVAL_MILLISECONDS);
        LocationServices.FusedLocationApi.requestLocationUpdates(mClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int cause) {
        mClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    private LocationController(Activity activity) {
        mActivity = new WeakReference<>(activity);

        mBuilder = new GoogleApiClient.Builder(activity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this);

        mClient = mBuilder.build();

        if (!isAccessFineLocationPermitted()) {
            requestAccessFineLocationPermission();
        }
    }

    private void notifyObservers(LatLng location) {
        setChanged();
        super.notifyObservers(location);
    }

    public LatLng getLastLocation() {
        return mLastLocation;
    }

    private void restoreLastLocation() {
        Context context = mClient.getContext();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        double latitude = Double.valueOf(settings.getString(KEY_LAT, VALUE_LAT));
        double longitude = Double.valueOf(settings.getString(KEY_LNG, VALUE_LNG));
        mLastLocation = new LatLng(latitude, longitude);
    }

    private void saveLastLocation() {
        Context context = mClient.getContext();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(KEY_LAT, Double.toString(mLastLocation.latitude));
        editor.putString(KEY_LNG, Double.toString(mLastLocation.longitude));
        editor.commit();
    }

    private boolean isAccessFineLocationPermitted() {
        // Beginning in Android 6.0 (API level 23), users grant permissions to apps while the
        // app is running, not when they install the app.
        int permissionCheck = ContextCompat.checkSelfPermission(mClient.getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        return PackageManager.PERMISSION_GRANTED == permissionCheck;
    }

    public void requestAccessFineLocationPermission() {
        Activity activity = mActivity.get();
        String permission = Manifest.permission.ACCESS_FINE_LOCATION;

        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
            Toast.makeText(activity, activity.getString(R.string.error_missing_permission), Toast.LENGTH_SHORT).show();
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{permission}, REQUEST_ACCESS_FINE_LOCATION);
        }
    }
}
