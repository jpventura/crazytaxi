package com.jpventura.crazytaxi.content.driver;

import android.net.Uri;
import android.provider.BaseColumns;

import com.jpventura.crazytaxi.content.DriverProvider;
import com.jpventura.crazytaxi.content.driver.DriverColumns;

/**
 * List of 99Taxi drivers.
 */
public class DriverColumns implements BaseColumns {
    public static final String TABLE_NAME = "driver";
    public static final Uri CONTENT_URI = Uri.parse(DriverProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    /**
     * Primary key.
     */
    public static final String _ID = BaseColumns._ID;

    public static final String DRIVER_ID = "driver_id";

    public static final String AVAILABLE = "available";

    public static final String LATITUDE = "latitude";

    public static final String LONGITUDE = "longitude";


    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            DRIVER_ID,
            AVAILABLE,
            LATITUDE,
            LONGITUDE
    };
    // @formatter:on

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (c.equals(DRIVER_ID) || c.contains("." + DRIVER_ID)) return true;
            if (c.equals(AVAILABLE) || c.contains("." + AVAILABLE)) return true;
            if (c.equals(LATITUDE) || c.contains("." + LATITUDE)) return true;
            if (c.equals(LONGITUDE) || c.contains("." + LONGITUDE)) return true;
        }
        return false;
    }

}
