package com.jpventura.crazytaxi.content.driver;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.jpventura.crazytaxi.content.base.AbstractSelection;

/**
 * Selection for the {@code driver} table.
 */
public class DriverSelection extends AbstractSelection<DriverSelection> {
    @Override
    protected Uri baseUri() {
        return DriverColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code DriverCursor} object, which is positioned before the first entry, or null.
     */
    public DriverCursor query(ContentResolver contentResolver, String[] projection) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new DriverCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, null)}.
     */
    public DriverCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null);
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param context The context to use for the query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code DriverCursor} object, which is positioned before the first entry, or null.
     */
    public DriverCursor query(Context context, String[] projection) {
        Cursor cursor = context.getContentResolver().query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new DriverCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(context, null)}.
     */
    public DriverCursor query(Context context) {
        return query(context, null);
    }


    public DriverSelection id(long... value) {
        addEquals("driver." + DriverColumns._ID, toObjectArray(value));
        return this;
    }

    public DriverSelection idNot(long... value) {
        addNotEquals("driver." + DriverColumns._ID, toObjectArray(value));
        return this;
    }

    public DriverSelection orderById(boolean desc) {
        orderBy("driver." + DriverColumns._ID, desc);
        return this;
    }

    public DriverSelection orderById() {
        return orderById(false);
    }

    public DriverSelection driverId(long... value) {
        addEquals(DriverColumns.DRIVER_ID, toObjectArray(value));
        return this;
    }

    public DriverSelection driverIdNot(long... value) {
        addNotEquals(DriverColumns.DRIVER_ID, toObjectArray(value));
        return this;
    }

    public DriverSelection driverIdGt(long value) {
        addGreaterThan(DriverColumns.DRIVER_ID, value);
        return this;
    }

    public DriverSelection driverIdGtEq(long value) {
        addGreaterThanOrEquals(DriverColumns.DRIVER_ID, value);
        return this;
    }

    public DriverSelection driverIdLt(long value) {
        addLessThan(DriverColumns.DRIVER_ID, value);
        return this;
    }

    public DriverSelection driverIdLtEq(long value) {
        addLessThanOrEquals(DriverColumns.DRIVER_ID, value);
        return this;
    }

    public DriverSelection orderByDriverId(boolean desc) {
        orderBy(DriverColumns.DRIVER_ID, desc);
        return this;
    }

    public DriverSelection orderByDriverId() {
        orderBy(DriverColumns.DRIVER_ID, false);
        return this;
    }

    public DriverSelection available(boolean value) {
        addEquals(DriverColumns.AVAILABLE, toObjectArray(value));
        return this;
    }

    public DriverSelection orderByAvailable(boolean desc) {
        orderBy(DriverColumns.AVAILABLE, desc);
        return this;
    }

    public DriverSelection orderByAvailable() {
        orderBy(DriverColumns.AVAILABLE, false);
        return this;
    }

    public DriverSelection latitude(double... value) {
        addEquals(DriverColumns.LATITUDE, toObjectArray(value));
        return this;
    }

    public DriverSelection latitudeNot(double... value) {
        addNotEquals(DriverColumns.LATITUDE, toObjectArray(value));
        return this;
    }

    public DriverSelection latitudeGt(double value) {
        addGreaterThan(DriverColumns.LATITUDE, value);
        return this;
    }

    public DriverSelection latitudeGtEq(double value) {
        addGreaterThanOrEquals(DriverColumns.LATITUDE, value);
        return this;
    }

    public DriverSelection latitudeLt(double value) {
        addLessThan(DriverColumns.LATITUDE, value);
        return this;
    }

    public DriverSelection latitudeLtEq(double value) {
        addLessThanOrEquals(DriverColumns.LATITUDE, value);
        return this;
    }

    public DriverSelection orderByLatitude(boolean desc) {
        orderBy(DriverColumns.LATITUDE, desc);
        return this;
    }

    public DriverSelection orderByLatitude() {
        orderBy(DriverColumns.LATITUDE, false);
        return this;
    }

    public DriverSelection longitude(double... value) {
        addEquals(DriverColumns.LONGITUDE, toObjectArray(value));
        return this;
    }

    public DriverSelection longitudeNot(double... value) {
        addNotEquals(DriverColumns.LONGITUDE, toObjectArray(value));
        return this;
    }

    public DriverSelection longitudeGt(double value) {
        addGreaterThan(DriverColumns.LONGITUDE, value);
        return this;
    }

    public DriverSelection longitudeGtEq(double value) {
        addGreaterThanOrEquals(DriverColumns.LONGITUDE, value);
        return this;
    }

    public DriverSelection longitudeLt(double value) {
        addLessThan(DriverColumns.LONGITUDE, value);
        return this;
    }

    public DriverSelection longitudeLtEq(double value) {
        addLessThanOrEquals(DriverColumns.LONGITUDE, value);
        return this;
    }

    public DriverSelection orderByLongitude(boolean desc) {
        orderBy(DriverColumns.LONGITUDE, desc);
        return this;
    }

    public DriverSelection orderByLongitude() {
        orderBy(DriverColumns.LONGITUDE, false);
        return this;
    }
}
