package com.jpventura.crazytaxi.content;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.DefaultDatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import com.jpventura.crazytaxi.BuildConfig;
import com.jpventura.crazytaxi.content.driver.DriverColumns;

public class DriverSQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String TAG = DriverSQLiteOpenHelper.class.getSimpleName();

    public static final String DATABASE_FILE_NAME = "crazy_taxi.db";
    private static final int DATABASE_VERSION = 1;
    private static DriverSQLiteOpenHelper sInstance;
    private final Context mContext;
    private final DriverSQLiteOpenHelperCallbacks mOpenHelperCallbacks;

    // @formatter:off
    public static final String SQL_CREATE_TABLE_DRIVER = "CREATE TABLE IF NOT EXISTS "
            + DriverColumns.TABLE_NAME + " ( "
            + DriverColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + DriverColumns.DRIVER_ID + " INTEGER NOT NULL, "
            + DriverColumns.AVAILABLE + " INTEGER NOT NULL DEFAULT 0, "
            + DriverColumns.LATITUDE + " REAL NOT NULL DEFAULT 0, "
            + DriverColumns.LONGITUDE + " REAL NOT NULL DEFAULT 0 "
            + ", CONSTRAINT unique_driver_id UNIQUE (driver_id) ON CONFLICT REPLACE"
            + " );";

    // @formatter:on

    public static DriverSQLiteOpenHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = newInstance(context.getApplicationContext());
        }
        return sInstance;
    }

    private static DriverSQLiteOpenHelper newInstance(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            return newInstancePreHoneycomb(context);
        }
        return newInstancePostHoneycomb(context);
    }


    /*
     * Pre Honeycomb.
     */
    private static DriverSQLiteOpenHelper newInstancePreHoneycomb(Context context) {
        return new DriverSQLiteOpenHelper(context);
    }

    private DriverSQLiteOpenHelper(Context context) {
        super(context, DATABASE_FILE_NAME, null, DATABASE_VERSION);
        mContext = context;
        mOpenHelperCallbacks = new DriverSQLiteOpenHelperCallbacks();
    }


    /*
     * Post Honeycomb.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static DriverSQLiteOpenHelper newInstancePostHoneycomb(Context context) {
        return new DriverSQLiteOpenHelper(context, new DefaultDatabaseErrorHandler());
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private DriverSQLiteOpenHelper(Context context, DatabaseErrorHandler errorHandler) {
        super(context, DATABASE_FILE_NAME, null, DATABASE_VERSION, errorHandler);
        mContext = context;
        mOpenHelperCallbacks = new DriverSQLiteOpenHelperCallbacks();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        if (BuildConfig.DEBUG) Log.d(TAG, "onCreate");
        mOpenHelperCallbacks.onPreCreate(mContext, db);
        db.execSQL(SQL_CREATE_TABLE_DRIVER);
        mOpenHelperCallbacks.onPostCreate(mContext, db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            setForeignKeyConstraintsEnabled(db);
        }
        mOpenHelperCallbacks.onOpen(mContext, db);
    }

    private void setForeignKeyConstraintsEnabled(SQLiteDatabase db) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            setForeignKeyConstraintsEnabledPreJellyBean(db);
        } else {
            setForeignKeyConstraintsEnabledPostJellyBean(db);
        }
    }

    private void setForeignKeyConstraintsEnabledPreJellyBean(SQLiteDatabase db) {
        db.execSQL("PRAGMA foreign_keys=ON;");
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setForeignKeyConstraintsEnabledPostJellyBean(SQLiteDatabase db) {
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        mOpenHelperCallbacks.onUpgrade(mContext, db, oldVersion, newVersion);
    }
}
