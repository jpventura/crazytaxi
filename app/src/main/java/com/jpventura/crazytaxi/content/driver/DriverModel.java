package com.jpventura.crazytaxi.content.driver;

import com.jpventura.crazytaxi.content.base.BaseModel;

import java.util.Date;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * List of 99Taxi drivers.
 */
public interface DriverModel extends BaseModel {

    /**
     * Get the {@code driver_id} value.
     */
    long getDriverId();

    /**
     * Get the {@code available} value.
     */
    boolean getAvailable();

    /**
     * Get the {@code latitude} value.
     */
    double getLatitude();

    /**
     * Get the {@code longitude} value.
     */
    double getLongitude();
}
