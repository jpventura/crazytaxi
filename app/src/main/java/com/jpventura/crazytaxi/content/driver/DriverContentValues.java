package com.jpventura.crazytaxi.content.driver;

import android.content.Context;
import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.jpventura.crazytaxi.backend.Driver;
import com.jpventura.crazytaxi.content.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code driver} table.
 */
public class DriverContentValues extends AbstractContentValues<Driver> {
    @Override
    public Uri uri() {
        return DriverColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, @Nullable DriverSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param context The context resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(Context context, @Nullable DriverSelection where) {
        return context.getContentResolver().update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public DriverContentValues putDriverId(long value) {
        mContentValues.put(DriverColumns.DRIVER_ID, value);
        return this;
    }


    public DriverContentValues putAvailable(boolean value) {
        mContentValues.put(DriverColumns.AVAILABLE, value);
        return this;
    }


    public DriverContentValues putLatitude(double value) {
        mContentValues.put(DriverColumns.LATITUDE, value);
        return this;
    }


    public DriverContentValues putLongitude(double value) {
        mContentValues.put(DriverColumns.LONGITUDE, value);
        return this;
    }

    @Override
    public DriverContentValues putValues(Driver driver) {
        putDriverId(driver.driverId);
        putAvailable(driver.driverAvailable);
        putLatitude(driver.latitude);
        putLongitude(driver.longitude);
        return this;
    }
}
