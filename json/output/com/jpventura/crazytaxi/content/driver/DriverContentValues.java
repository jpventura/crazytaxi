package com.jpventura.crazytaxi.content.driver;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.jpventura.crazytaxi.content.base.AbstractContentValues;

/**
 * Content values wrapper for the {@code driver} table.
 */
public class DriverContentValues extends AbstractContentValues {
    @Override
    public Uri uri() {
        return DriverColumns.CONTENT_URI;
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(ContentResolver contentResolver, @Nullable DriverSelection where) {
        return contentResolver.update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    /**
     * Update row(s) using the values stored by this object and the given selection.
     *
     * @param contentResolver The content resolver to use.
     * @param where The selection to use (can be {@code null}).
     */
    public int update(Context context, @Nullable DriverSelection where) {
        return context.getContentResolver().update(uri(), values(), where == null ? null : where.sel(), where == null ? null : where.args());
    }

    public DriverContentValues putDriverId(long value) {
        mContentValues.put(DriverColumns.DRIVER_ID, value);
        return this;
    }


    public DriverContentValues putAvailable(boolean value) {
        mContentValues.put(DriverColumns.AVAILABLE, value);
        return this;
    }


    public DriverContentValues putEast(double value) {
        mContentValues.put(DriverColumns.EAST, value);
        return this;
    }


    public DriverContentValues putSouth(double value) {
        mContentValues.put(DriverColumns.SOUTH, value);
        return this;
    }


    public DriverContentValues putNorth(double value) {
        mContentValues.put(DriverColumns.NORTH, value);
        return this;
    }


    public DriverContentValues putWest(double value) {
        mContentValues.put(DriverColumns.WEST, value);
        return this;
    }

}
