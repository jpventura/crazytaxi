package com.jpventura.crazytaxi.content.driver;

import android.net.Uri;
import android.provider.BaseColumns;

import com.jpventura.crazytaxi.content.DriverProvider;
import com.jpventura.crazytaxi.content.driver.DriverColumns;

/**
 * List of 99Taxi drivers.
 */
public class DriverColumns implements BaseColumns {
    public static final String TABLE_NAME = "driver";
    public static final Uri CONTENT_URI = Uri.parse(DriverProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    /**
     * Primary key.
     */
    public static final String _ID = BaseColumns._ID;

    public static final String DRIVER_ID = "driver_id";

    public static final String AVAILABLE = "available";

    public static final String EAST = "east";

    public static final String SOUTH = "south";

    public static final String NORTH = "north";

    public static final String WEST = "west";


    public static final String DEFAULT_ORDER = TABLE_NAME + "." +_ID;

    // @formatter:off
    public static final String[] ALL_COLUMNS = new String[] {
            _ID,
            DRIVER_ID,
            AVAILABLE,
            EAST,
            SOUTH,
            NORTH,
            WEST
    };
    // @formatter:on

    public static boolean hasColumns(String[] projection) {
        if (projection == null) return true;
        for (String c : projection) {
            if (c.equals(DRIVER_ID) || c.contains("." + DRIVER_ID)) return true;
            if (c.equals(AVAILABLE) || c.contains("." + AVAILABLE)) return true;
            if (c.equals(EAST) || c.contains("." + EAST)) return true;
            if (c.equals(SOUTH) || c.contains("." + SOUTH)) return true;
            if (c.equals(NORTH) || c.contains("." + NORTH)) return true;
            if (c.equals(WEST) || c.contains("." + WEST)) return true;
        }
        return false;
    }

}
