package com.jpventura.crazytaxi.content.driver;

import java.util.Date;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.jpventura.crazytaxi.content.base.AbstractCursor;

/**
 * Cursor wrapper for the {@code driver} table.
 */
public class DriverCursor extends AbstractCursor implements DriverModel {
    public DriverCursor(Cursor cursor) {
        super(cursor);
    }

    /**
     * Primary key.
     */
    public long getId() {
        Long res = getLongOrNull(DriverColumns._ID);
        if (res == null)
            throw new NullPointerException("The value of '_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code driver_id} value.
     */
    public long getDriverId() {
        Long res = getLongOrNull(DriverColumns.DRIVER_ID);
        if (res == null)
            throw new NullPointerException("The value of 'driver_id' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code available} value.
     */
    public boolean getAvailable() {
        Boolean res = getBooleanOrNull(DriverColumns.AVAILABLE);
        if (res == null)
            throw new NullPointerException("The value of 'available' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code east} value.
     */
    public double getEast() {
        Double res = getDoubleOrNull(DriverColumns.EAST);
        if (res == null)
            throw new NullPointerException("The value of 'east' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code south} value.
     */
    public double getSouth() {
        Double res = getDoubleOrNull(DriverColumns.SOUTH);
        if (res == null)
            throw new NullPointerException("The value of 'south' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code north} value.
     */
    public double getNorth() {
        Double res = getDoubleOrNull(DriverColumns.NORTH);
        if (res == null)
            throw new NullPointerException("The value of 'north' in the database was null, which is not allowed according to the model definition");
        return res;
    }

    /**
     * Get the {@code west} value.
     */
    public double getWest() {
        Double res = getDoubleOrNull(DriverColumns.WEST);
        if (res == null)
            throw new NullPointerException("The value of 'west' in the database was null, which is not allowed according to the model definition");
        return res;
    }
}
