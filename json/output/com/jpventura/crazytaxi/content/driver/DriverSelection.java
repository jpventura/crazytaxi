package com.jpventura.crazytaxi.content.driver;

import java.util.Date;

import android.content.Context;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;

import com.jpventura.crazytaxi.content.base.AbstractSelection;

/**
 * Selection for the {@code driver} table.
 */
public class DriverSelection extends AbstractSelection<DriverSelection> {
    @Override
    protected Uri baseUri() {
        return DriverColumns.CONTENT_URI;
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param contentResolver The content resolver to query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code DriverCursor} object, which is positioned before the first entry, or null.
     */
    public DriverCursor query(ContentResolver contentResolver, String[] projection) {
        Cursor cursor = contentResolver.query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new DriverCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(contentResolver, null)}.
     */
    public DriverCursor query(ContentResolver contentResolver) {
        return query(contentResolver, null);
    }

    /**
     * Query the given content resolver using this selection.
     *
     * @param context The context to use for the query.
     * @param projection A list of which columns to return. Passing null will return all columns, which is inefficient.
     * @return A {@code DriverCursor} object, which is positioned before the first entry, or null.
     */
    public DriverCursor query(Context context, String[] projection) {
        Cursor cursor = context.getContentResolver().query(uri(), projection, sel(), args(), order());
        if (cursor == null) return null;
        return new DriverCursor(cursor);
    }

    /**
     * Equivalent of calling {@code query(context, null)}.
     */
    public DriverCursor query(Context context) {
        return query(context, null);
    }


    public DriverSelection id(long... value) {
        addEquals("driver." + DriverColumns._ID, toObjectArray(value));
        return this;
    }

    public DriverSelection idNot(long... value) {
        addNotEquals("driver." + DriverColumns._ID, toObjectArray(value));
        return this;
    }

    public DriverSelection orderById(boolean desc) {
        orderBy("driver." + DriverColumns._ID, desc);
        return this;
    }

    public DriverSelection orderById() {
        return orderById(false);
    }

    public DriverSelection driverId(long... value) {
        addEquals(DriverColumns.DRIVER_ID, toObjectArray(value));
        return this;
    }

    public DriverSelection driverIdNot(long... value) {
        addNotEquals(DriverColumns.DRIVER_ID, toObjectArray(value));
        return this;
    }

    public DriverSelection driverIdGt(long value) {
        addGreaterThan(DriverColumns.DRIVER_ID, value);
        return this;
    }

    public DriverSelection driverIdGtEq(long value) {
        addGreaterThanOrEquals(DriverColumns.DRIVER_ID, value);
        return this;
    }

    public DriverSelection driverIdLt(long value) {
        addLessThan(DriverColumns.DRIVER_ID, value);
        return this;
    }

    public DriverSelection driverIdLtEq(long value) {
        addLessThanOrEquals(DriverColumns.DRIVER_ID, value);
        return this;
    }

    public DriverSelection orderByDriverId(boolean desc) {
        orderBy(DriverColumns.DRIVER_ID, desc);
        return this;
    }

    public DriverSelection orderByDriverId() {
        orderBy(DriverColumns.DRIVER_ID, false);
        return this;
    }

    public DriverSelection available(boolean value) {
        addEquals(DriverColumns.AVAILABLE, toObjectArray(value));
        return this;
    }

    public DriverSelection orderByAvailable(boolean desc) {
        orderBy(DriverColumns.AVAILABLE, desc);
        return this;
    }

    public DriverSelection orderByAvailable() {
        orderBy(DriverColumns.AVAILABLE, false);
        return this;
    }

    public DriverSelection east(double... value) {
        addEquals(DriverColumns.EAST, toObjectArray(value));
        return this;
    }

    public DriverSelection eastNot(double... value) {
        addNotEquals(DriverColumns.EAST, toObjectArray(value));
        return this;
    }

    public DriverSelection eastGt(double value) {
        addGreaterThan(DriverColumns.EAST, value);
        return this;
    }

    public DriverSelection eastGtEq(double value) {
        addGreaterThanOrEquals(DriverColumns.EAST, value);
        return this;
    }

    public DriverSelection eastLt(double value) {
        addLessThan(DriverColumns.EAST, value);
        return this;
    }

    public DriverSelection eastLtEq(double value) {
        addLessThanOrEquals(DriverColumns.EAST, value);
        return this;
    }

    public DriverSelection orderByEast(boolean desc) {
        orderBy(DriverColumns.EAST, desc);
        return this;
    }

    public DriverSelection orderByEast() {
        orderBy(DriverColumns.EAST, false);
        return this;
    }

    public DriverSelection south(double... value) {
        addEquals(DriverColumns.SOUTH, toObjectArray(value));
        return this;
    }

    public DriverSelection southNot(double... value) {
        addNotEquals(DriverColumns.SOUTH, toObjectArray(value));
        return this;
    }

    public DriverSelection southGt(double value) {
        addGreaterThan(DriverColumns.SOUTH, value);
        return this;
    }

    public DriverSelection southGtEq(double value) {
        addGreaterThanOrEquals(DriverColumns.SOUTH, value);
        return this;
    }

    public DriverSelection southLt(double value) {
        addLessThan(DriverColumns.SOUTH, value);
        return this;
    }

    public DriverSelection southLtEq(double value) {
        addLessThanOrEquals(DriverColumns.SOUTH, value);
        return this;
    }

    public DriverSelection orderBySouth(boolean desc) {
        orderBy(DriverColumns.SOUTH, desc);
        return this;
    }

    public DriverSelection orderBySouth() {
        orderBy(DriverColumns.SOUTH, false);
        return this;
    }

    public DriverSelection north(double... value) {
        addEquals(DriverColumns.NORTH, toObjectArray(value));
        return this;
    }

    public DriverSelection northNot(double... value) {
        addNotEquals(DriverColumns.NORTH, toObjectArray(value));
        return this;
    }

    public DriverSelection northGt(double value) {
        addGreaterThan(DriverColumns.NORTH, value);
        return this;
    }

    public DriverSelection northGtEq(double value) {
        addGreaterThanOrEquals(DriverColumns.NORTH, value);
        return this;
    }

    public DriverSelection northLt(double value) {
        addLessThan(DriverColumns.NORTH, value);
        return this;
    }

    public DriverSelection northLtEq(double value) {
        addLessThanOrEquals(DriverColumns.NORTH, value);
        return this;
    }

    public DriverSelection orderByNorth(boolean desc) {
        orderBy(DriverColumns.NORTH, desc);
        return this;
    }

    public DriverSelection orderByNorth() {
        orderBy(DriverColumns.NORTH, false);
        return this;
    }

    public DriverSelection west(double... value) {
        addEquals(DriverColumns.WEST, toObjectArray(value));
        return this;
    }

    public DriverSelection westNot(double... value) {
        addNotEquals(DriverColumns.WEST, toObjectArray(value));
        return this;
    }

    public DriverSelection westGt(double value) {
        addGreaterThan(DriverColumns.WEST, value);
        return this;
    }

    public DriverSelection westGtEq(double value) {
        addGreaterThanOrEquals(DriverColumns.WEST, value);
        return this;
    }

    public DriverSelection westLt(double value) {
        addLessThan(DriverColumns.WEST, value);
        return this;
    }

    public DriverSelection westLtEq(double value) {
        addLessThanOrEquals(DriverColumns.WEST, value);
        return this;
    }

    public DriverSelection orderByWest(boolean desc) {
        orderBy(DriverColumns.WEST, desc);
        return this;
    }

    public DriverSelection orderByWest() {
        orderBy(DriverColumns.WEST, false);
        return this;
    }
}
